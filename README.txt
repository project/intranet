

INTRANET MODULE 
================================================================================

author: Juan Vizcarrondo  (jvizcarrondo) http://wiki.cenditel.gob.ve/wiki/jvizcarrondo

This module allow manage a group of workers within Drupal.


Install
================================================================================
1. Copy the folder intranet inside the  directory of your site sites/all/modules.
2. Enter in Administer --> Site building -> Modules (admin/build/modules) and enable the module.

Settings
================================================================================
1. Configure the module in Administer -> Site configuration -> Intranet Settings (admin/settings/intranet).
2. Include users who were part of the intranet, for this select intranet option "Intranet Access" in the form of user data ('admin/user/user/create', 'user/uid/edit&destination=admin%2Fuser%2Fuser'.
3. Add the rest of the information necessary for the user in 'intranet/employee'.
4. Grant permissions to users.

New HOOK
================================================================================
1. menu_intranet: Allows add a new menu of options available on the homepage of the intranet. The data return should be an array of the form array('name'=>MENU NAME,'description'=>MENU DESCRIPTION,'call'=>HOOK THE SUMMONS TO THE NEW MENU).
2. menu_intranet_MODULE: Allows you to add a new option to a previously defined menu on the homepage of the intranet. MODULE orresponds to the module that implements the menu. The data return should be an array of the form array('name'=>OPTION NAME,'path'=>PATH TO THE OPTION,'description'=>OPTION DESCRIPTION)
3. employee_intranet: Use this hook to add information to workers. The input is the object that contains the information worker. Additionally, return data is a string that will be concatenated with the output of the system.

CREDITS
================================================================================

Intranet 
  juan Vizcarrondo <jvizcarrondo [ARROBA] cenditel.gob.ve> (2007, 2009 CENDITEL)

